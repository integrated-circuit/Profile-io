+++
title = 'Kicad'
date = 2024-09-14T12:37:55+02:00
draft = false
+++

## Preface

When studying electronics, it's only a matter of time until you'll have to go from a project on a breadboard to a complete PCB. 

To do that, you will need to use an [EDA](https://en.wikipedia.org/wiki/Electronic_design_automation) software to design your board. One of the major players in the open-source world is [KiCad](https://www.kicad.org/). It's a all-in-one suite of software to design schematics, PCBs, symbols or footprints, visualizing Gerber files, do component calculations and test components. *yeez that was a mouthful*  

However not all components are included and the install package[^1]. When that happens, you've got two options depending on your skill level:

1. Use a service like UltraLibrarian or SnapEDA to get a pre-made part footprint/ symbol. This is the easiest way to pass that roadblock if you just started using KiCad. As for 3D models, check model sharing sites like GrabCAD and others.
2. Create the part from scratch by yourself. This is mostly reserved for advanced users (ie. People who already have an understanding of KiCad) as you will have to know how to get correct information from data-sheets. You'll also have to use external tools like FreeCAD/ Git if you want to make a 3D model[^2] and contribute back to the Library. 
   
I suggest reading the [Getting Started Guide ](https://docs.kicad.org/8.0/en/getting_started_in_kicad/getting_started_in_kicad.html) for tips on how to create your part in a KiCad-format. Try one or two simple test parts before creating an **actual** part as this will allow to get some experience before tackling more complex ones.

*Small PSA, if you go to the trouble of creating the part, please consider adding it to the KiCad library so that other users can benefit from your hard work.*


You can see below some of the contributions I made over the years to the KiCad Library.

{{< gallery globalMatch="images/gallery/kicad/**" sortOrder="asc" rowHeight="150" margins="10" thumbnailResizeOptions="600x600 q90 Lanczos" showExif=true previewType="blur" embedPreview=true thumbnailHoverEffect=enlarge loadJQuery=true >}}

[^1]: This is an issue other FOSS and closed-source/ proprietary EDAs face due to the sheer volume of new silicon being continuously released. In contrary to KiCad, professional EDAs have teams that can react faster than us and add the corresponding packages to their software.\
This is changing however as we are seeing [more](https://gitlab.com/techsupport_traco) and [more](https://www.digikey.com/en/resources/design-tools/kicad) brands trying to contribute directly to the KiCad libraries to give electronic engineers an out-off-the box experience.

[^2]: ⚠ 3D models are notoriously bad to create as you have to deal with tolerances, difficult shapes and complexity from poor manufacturer-provided information, etc... . This is regarded as a difficult achievement in the Librarians community, so please pick something easier to start before having a burn-out because your model takes **50 REVISIONS**  before being correct. ;) ⚠