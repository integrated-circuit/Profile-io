+++
title = 'Liftoff!'
date = 2023-04-11T19:21:54+02:00
draft = false

thumbnail = "thumbnails/liftoff.svg"
+++
*Yay, Content!*

![A terminal window showing a joke program about 'hello world'](/images/liftoff/term.svg "A terminal window showing a joke program about 'hello world'")

This has been a hell of a ride, but I'm very proud to announce that my website is now officially **open**. \
I've found enough time to maintain and update all the code. Some sections are scarce thought, but rest assured that more content will be added in the following months. 

As stated in the sidebar description, you can expect EE, talks about Linux and some repairability thoughts.

First project will however be of limited scale as I am currently learning Electronics Design as part of my [Tri-nat curriculum](https://formations.unistra.fr/fr/formations/but-BT/but-genie-electrique-et-informatique-industrielle-ME342/electronique-et-systemes-embarques-trinational-PR1535.html) .