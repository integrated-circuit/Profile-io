+++
title = 'My 2 cents on the PineTime'
date = 2024-09-10T10:22:21+02:00
draft = false
tags = ["open source", "wearables"]
toc = true
thumbnail = "thumbnails/pinetime.svg"
+++
*My watch runs Linux now*

![A PineTime running InfiniTime](/images/pinetime/pinetime.png "A PineTime running InfiniTime")


I've recently got a PineTime from [Pine64's EU store](https://pine64eu.com/). In this blog post, I'm going to talk about my experience with it.

## What's a PineTime?
The PineTime is a Free and Open-Source (FOSS) smartwatch. This means that everything from its code to its schematic is available freely and without vendor lock-in. 

This philosophy also reflect in the companion(s) app(s) being used, as there are multiple ones available. \
In no particular order : 
    
1. [Gadgetbridge](https://gadgetbridge.org/), a general purpose Android app for wearables.
2. [Amazfish](https://openrepos.net/content/piggz/amazfish/), a SailfishOS app.
3. [Siglo](https://github.com/alexr4535/siglo), a GTK 3 Linux app
4. [InfiniLink](https://github.com/InfiniTimeOrg/InfiniLink), an iOS app, who recently got a [new maintainer](https://pine64.org/2024/03/17/march-update-making-waves/#pinetime).
5. [ITD](https://gitea.elara.ws/Elara6331/itd), a Linux CLI app.
6. [WatchMate](https://github.com/azymohliad/watchmate), a Linux GTK 4 app.


## So it runs Linux?
Not quite. The PineTime runs by default an OS called [InfiniTime](https://github.com/InfiniTimeOrg/InfiniTime) (but since the watch is FOSS, there are multiple OS options, more on that later).\
InfiniTime is programmed using C++ and based on FreeRTOS. 

*To better explain the difference between Linux and FreeRTOS, we are going to do a little tangent on OS types*.
### FreeRTOS?
FreeRTOS is a FOSS **RTOS** (aka Real Time Operating System). The main difference/advantage is that FreeRTOS is designed to provide real-time reactivity for embedded devices. this is achieved by using a scheduler with predictable output with a fixed time-for-execution. \
For more details, here is a quote from the [FreeRTOS website](https://www.freertos.org/Why-FreeRTOS/What-is-FreeRTOS#what-is-an-rtos) explaining the differences of the Linux and RTOS scheduler:

>The type of an operating system is defined by how the scheduler decides which program to run when. For example, [...] the scheduler in a desktop operating system will try and ensure the computer remains responsive to its user. [...]\
>The scheduler in a Real Time Operating System is designed to provide a predictable (normally described as deterministic) execution pattern. This is particularly of interest to embedded systems as embedded systems often have real time requirements.\
>A real time requirements is one that specifies that the embedded system must respond to a certain event within a strictly defined time (the deadline). A guarantee to meet real time requirements can only be made if the behavior of the operating system's scheduler can be predicted (and is therefore deterministic).

Thanks to RTOSes, embedded systems can more reliable and stable, while also using less resources (ROM/Flash,RAM), than their desktop counterparts.

As for the stability of the OS when daily-driving it, I haven't had major crashes in the last month and the few I got since using it (around 3~4 crashes on a ≃3 months period) were due to [user error]( {{< ref "pinetime.md#gadgetbridge-quirks" >}} ).

## Looks great! Where can I get one?
**Not so fast.**
There are some quirks that you need to be aware before buying it. These can be grouped in two categories:  

### Gadgetbridge quirks 

##### Impossible to connect / pair
- After switching my phone to a Pixel 8a, my PineTime wouldn't pair like before. At first, I assumed that the Bluetooth link between my old phone and the watch still existed, so I force-restated the watch to delete it.\
This didn't changed anything, which made me wonder if permissions of the app weren't correctly set.\
Since everything was OK, I assumed that the watch was dead. I was starting to ask for a replacement from the store when someone from the PineTime community pointed me to [this article](https://gadgetbridge.org/basics/topics/background-service/) who fixed the issue that I had.

   Basically, (Vanilla) Android 12 has changed where the background activity setting was. To enable it, you have to go to the app settings, then App battery usage and you should have a slider to set it up.      

### PineTime quirks

##### INFINITE ENERGY!
- When using the PineTime for the first month, I've had the pleasure to see that the battery apparently has *Infinite Energy*, as the watch continued to work for **3 days** after the battery percentage hit 0%!

*Jokes aside, this is normal. If you ever designed electronic devices, you might already know where this is going. But let's go on a little tangent on battery discharge curves for the uninitiated , shall we? (I promise it's the last one)* :crossed_fingers:

When you are designing a embedded device, the two most important metric are:

1. Is the micro-controller (µC) adequate to the task in terms of raw power/peripherals/supported protocols/etc...
2. And is my design battery efficient, ie can the  device stay powered more than 10 minutes

These two overlap themselves (as you can imagine, you cannot power a supercomputer wit AAA batteries) and you need to find an equilibrium between these two.\
This is were the battery discharge curves intervene. These serve as limiting voltage values that the system can drain before shutting himself down to protect the battery from over-discharge, wear or anode/cathode degradation.[^1]

If you followed until that and are be thinking: *Wait, this is bad! This means that the PineTime battery is designed to always fail! This product is e-Waste!* 

Let's calm and break down what happens here. The culprits in this situation are [the battery discharge curves PR](https://github.com/InfiniTimeOrg/InfiniTime/pull/1494) that have been added in the [1.12.0](https://github.com/InfiniTimeOrg/InfiniTime/milestone/12) release and the SoC being used here.

In the pull request, the values used to calculate the battery levels were tweaked to be more precise and allow prolonged battery life, especially when being at 50% capacity. 

To take into account that some PineTime have seen their battery degrade as part of normal operation, these values had a bit of room, allowing used and new watches to have a (mostly) similar uptime.

This was explained by [Neroburner](https://github.com/NeroBurner), a developer of the InfiniTime project, when I asked them about the (seemingly infinite) battery capacity:

>The percentage is a matching of the non linear battery voltage to a linear percentage value. We added some padding at the 100% and the 0% to be sure to hit 0% on all batteries instead of running out of juice earlier. So it is fine if you hit 0%. Just a sign that your battery is healthy :smile:

So no need to worry. In fact when I asked the rest of the community, multiple users told me that they had their watch run for **more than 3 days** after hitting 0%.

This efficiency is also *partly* due to the µC used. The Nordic Semiconductor series, the nRF52832 to be more precise, is commonly used in similar wearables and uses **0.3 µA (0,00000003 A)** in deep sleep (*Holly cow!*). Combine that with a 180 mAh battery and you have a pretty efficient device.  

Nordic markets this µC as having a flexible power management, and I can say with confidence that the InfiniTime developers tapped into it:
>Flexible power management:
>    - 1.7 V–3.6 V supply voltage range
>    - Fully automatic LDO and DC/DC regulator system
>    - Fast wake-up using 64 MHz internal oscillator
>    - 0.3 μA at 3 V in System OFF mode
>    - 0.7 μA at 3 V in System OFF mode with full 64 kB RAM retention
>   - 1.9 μA at 3 V in System ON mode, no RAM retention, wake on RTC


## So... Is it worth it?
*Warning, this part is opinionated are the ideas that are expressed are my owns. Please take them with a great of salt before buying this product and make sure that it fits your need. E-Waste pollution is an important problem and has to be taken in consideration when buying an electronic device.*

As I said before, this watch target audience is mainly hackers/people who aren't afraid of tinkering with their hardware.

Never the less, the work done by these people have allowed to have a FOSS OS that is very usable, even for newbies like me, and integrates a lot of functionality (media control, footstep tracking, Hearth Rate (HR) sensing, Navigation and more). 

My experience with the watch has been so good that it replaced my [Huawei GT2e](https://www.gsmarena.com/huawei_watch_gt_2e-10166.php), offers most the former one features while being cheaper, having a better battery life, being vendor-lock free and FOSS.

It's for me a total win/win scenario, it is such a departure from the always-tracking watches produced. I would recommend this watch if you are interested in technology and  are interested to see how devices work under the hood.     
[^1]: Please be aware that I am simplifying a lot of elements regarding the batteries inner workings. I excluded things like capacity change caused by internal and environmental temperature variations.