+++
title = 'Migration from plain HTML to Hugo'
date = 2025-03-10T10:23:15+02:00
draft = false
toc = true
thumbnail = "thumbnails/friendship ended.svg"
tags = ["open source", "hugo"]
+++
*Friendship ended with plain HTML / Now Hugo + Markdown is my best friend.*

![The Hugo project logo © Steve Francia 2013-2025](/images/migration/hugo_logo.svg "The Hugo Logo by Steve Francia and Renée French")

I've until now mainly used plain HTML to create my site. It was however becoming increasingly difficult to maintain as I had to create the structure by hand and add links for *every* new article.\
I've since them transitioned to the [Hugo Static Site Generator (SSG)](https://gohugo.io) after checking out the [TWIG](https://thisweek.gnome.org/) blog and wanting to have a blog with short iteration times.

This has allowed me to change from a painful process to something similar to word processing and lowers the production time from around 1 week (writing, importing external content, styling,...) to a couple of days.\
In this blogpost, I am going to explain what Hugo is, what jumps and hoops I've had to go through to migrate my site, what was easy and what wasn't.

## What is a SSG?

Per the [Awesome Static Web Site Generators List](https://github.com/myles/awesome-static-generators):
> A static web site generator is an application that takes plain text files and compiles them to HTML files.

In a nutshell an SSG has to parse static textfiles (mostly) written in human readable text and turn it into hostable HTML. 
Static textfiles here refer to *.txt, *.md, *.rst, and others.

The main advantage is to allow website creation to be easier and faster, while also allowing the user to use custom HTML components if he wants to. 
To do this, SSG apply the [Don't Repeat Yourself (DRY)](https://en.wikipedia.org/wiki/Don't_repeat_yourself) method and allow you to create site as a bunch of modules that you can reuse and import instead rewriting everything each time.

This is very useful for blogs, complex websites like digital libraries, etc.. that require a lot of duplicated code.

## What is Hugo?
Hugo is a [Open-Source](https://github.com/gohugoio/hugo) SSG using the [Go language](https://github.com/gohugoio/hugo) originally created by Steve Francia (aka [@spf13](https://github.com/spf13)) and maintained as of today by Bjørn Erik Pedersen (aka [@bep](https://github.com/bep)) and a [bunch of other contributors](https://github.com/gohugoio/hugo/graphs/contributors).\
The main advertised features are speed and flexibility. 


## Hugo site VS HTML site

### The Good
- Hugo allowed me to switch from writing and structuring HTML to simply posting [Markdown](https://daringfireball.net/projects/markdown/). This makes [Links, Images, Bold and italic text](https://www.markdownguide.org/cheat-sheet/) a lot more easier to integrate to the site.\
   Previously, I spent a lot of time copying and pasting the structure between new articles (around 30% of the time it took to write the article was styling, setting up the ``` <div></div>```'s, ...). 
- Hugo is a speedy SSG, which means sites can be built and deployed faster than raw HTML.\
Based on tests on my own site ([Hugo's internal metrics tool](https://gohugo.io/troubleshooting/performance/) + Gitlab CI time), Hugo is around 1,2 times faster (~ **150%**):

    > *[That's a nice argument, Senator — why don't you back it up with a source?](https://www.youtube.com/watch?v=wMD538aNBTw&t=0s)*

    Okay but more seriously, here are my site performance metrics as of today:
    

    Template Metrics:

    |cumulative  duration    | average duration     | maximum duration  |cache potential   |percent cached   |cached count  | total count|  template |
    |-------------|--------------|------------|-----------|---------|--------|------|----------|
    | 1.746423945s |  97.023552ms | 1.515849538s   |      63    |    0   |    0   |  18 | partials/head.html|
    | 1.576550155s | 315.310031ms |  1.51682447s   |       0    |    0   |    0   |   5 | _default/single.html|
    | 1.485952362s | 1.485952362s | 1.485952362s   |       0    |    0   |    0   |   1 | shortcodes/gallery.html|
    | 142.984015ms |  14.298401ms |  35.934322ms   |       0    |    0   |    0   | 10  |_default/list.html|
    |  59.703933ms |  59.703933ms |  59.703933ms   |       0    |    0   |    0   |   1 | index.html|
    |  41.047576ms |  41.047576ms |  41.047576ms   |       0    |    0   |    0   |   1|  404.html|
    |  35.889756ms |  1.993875ms  |  6.276833ms    |     97     |   0    |   0    | 18 | partials/sidebar.html|
    |  28.756096ms |     798.78µs |   6.019722ms   |      98    |    0   |    0   |  36|  partials/footer.html|
    |  17.295303ms |     960.85µs |   6.217508ms   |      66    |    0   |    0   |  18|  partials/schema.html|
    |   7.107267ms |    197.424µs |   3.859493ms   |     100    |    0   |    0   |  36|  partials/medium-zoom.html|
    |   6.103892ms |    169.552µs |  1.084886ms    |    100     |   0    |   0    | 36 | partials/_funcs/get-page-images.html|
    |   4.693051ms |    260.725µs |   1.106651ms   |      97    |    0   |    0   |  18|  partials/navbar.html|
    |   3.942286ms |   1.314095ms |    2.64515ms   |       0    |    0   |    0   |   3|  _default/_markup/render-image.html|
    |   3.122758ms |    260.229µs |    503.954µs   |       0    |    0   |    0   |  12|  _default/rss.xml|
    |   2.426019ms |   2.426019ms |   2.426019ms   |       0    |    0   |    0   |   1|  portfolio/list.html|
    |    1.29394ms |     35.942µs |    362.525µs   |     100    |    0   |    0   |  36|  partials/math.html|
    |   1.135572ms |   1.135572ms |   1.135572ms   |       0    |    0   |    0   |   1|  _internal/_default/sitemap.xml|
    |    215.167µs |     43.033µs |    161.292µs   |     100    |    0   |    0   |   5|  partials/expirationnote.html|
    |    205.362µs |    102.681µs |   155.551µs    |    100     |   0    |   0    |  2 | partials/taxonomy/tags.html|
    |    133.736µs |     66.868µs |     94.302µs   |     100    |    0   |    0   |   2|  partials/taxonomy/template.html|
    |     90.642µs |     90.642µs |     90.642µs   |       0    |    0   |    0   |   1|  _internal/shortcodes/ref.html|
    |      80.36µs |      80.36µs |      80.36µs   |       0    |    0   |    0   |   1|  _internal/alias.html|
    |      57.04µs |      57.04µs |      57.04µs   |     100    |    0   |    0   |   1|  partials/toc.html|
    |     54.726µs |     54.726µs |     54.726µs   |       0    |    0   |    0   |   1|  _internal/_default/robots.txt|
    |     15.583µs |     15.583µs |     15.583µs   |       0    |    0   |    0   |  1 | /scss/anatole.scss|

    As for the CI Time, here it is:

    | Tool   |Nbr of pages   | Time to deploy |
    | -------------|---------------|----------------|
    | Hugo       |   8            | 25 sec          |
    | Raw HTML    | 3             |   30 sec           |

    *Keep in mind that Hugo has to transcode the Markdown files to HTML and bundle the assets (images, JS, etc.. ) as opposed to just existing for the HTML. So yeah it's fast.*



- The Theme system allowed me to have a base that fits my style without starting from scratch. (Thanks [Alex](http://www.alexbilz.com/) for the [Anatole Theme](https://github.com/lxndrblz/anatole)). 
And even if something in the theme isn't to my liking, I can edit the CSS or set custom fonts.
### The "Meh"
- Using custom components requires you to learn how to create [shortcodes](https://gohugo.io/templates/shortcode/#article) and [partials](https://gohugo.io/templates/partial/#article). This requires a bit of knowledge of how Hugo works but after your first use, it's relatively easy.\
Example for me is a custom shortcode integrating [KiCanvas](https://kicanvas.org/home/) to embed/display KiCad files for a future project 😉.

### The Bad/Difficult
- There are some nasty bugs like [Upper case letters in path causing match to fail](https://github.com/gohugoio/hugo/issues/7686) that aren't obvious at first. This one is sadly due to the way case-sensitive OS (like Linux) and  case-insensitive OS (e.g., Windows) implement their directory API. But yeah, when you wonder why your 9999+ files aren't built by Hugo, you start to question your life choices.

## Conclusion
Even with it's few faults, I really appreciate Hugo, as it only takes a few minutes to create a blog post instead of the days for HTML. I can concentrate on more demanding project since I know that sharing my findings will (mostly) be a turn-key solution 

This also enables me to contribute to the blog despite my busy student schedule. And for that I like it!