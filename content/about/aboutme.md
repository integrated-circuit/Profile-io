+++
title = 'About Me'
date = 2024-09-12T21:40:23+02:00
draft = false
+++


![A photo of me](/images/about/profile-photo.png)

Hi, I'm [Int-Circuit](https://gitlab.com/integrated-circuit), an Engineering Student in electronics who does a little bit of web design. This blog serves to document my projects, or chat about things that interest me. You also can expect talks about Right to Repair (2R), Hardware and Software projects and posts about Linux/Open Source.

🧑‍💻   I started programming and Linux'ing since my Junior Year. When I'm not hacking the planet, I'm contributing to the KiCad Symbol, Footprints and 3D models libraries for better [OSHW](https://www.oshwa.org/) devices. 

🖥️   I code in C/C++, HTML, CSS.

📫   You can contact me using the socials in the sidebar. For questions and general chatting, please use [Matrix](https://matrix.to/#/@integrated-circuit:matrix.org).



