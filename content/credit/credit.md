+++
title = 'Credits'
date = 2024-11-13T12:37:55+02:00
draft = false
+++

This page serves as acknowledgements to the FOSS projects used to build/ run/ maintain this website. If you like what you saw, please consider donating or contributing to them so that they can be improved further 😄.

In no particular order, I would like to thank:
    
1. [Hugo](https://gohugo.io/) by the [Hugo authors](https://github.com/gohugoio/hugo/graphs/contributors)
2. [Anatole Hugo theme](https://github.com/lxndrblz/anatole) by [Alexander Bilz](http://www.alexbilz.com/)
3. [Hugo-shortcode-gallery](https://github.com/mfg92/hugo-shortcode-gallery) by [mfg92](https://matze.rocks/)


I'm very grateful for the continuous work you put in your projects, hope that all goes well for you.